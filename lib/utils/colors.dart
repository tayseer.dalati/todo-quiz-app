import 'dart:ui';

import 'package:flutter/widgets.dart';

const WHITE = Color.fromARGB(255, 255, 255, 255);
const OFF_WHITE = Color.fromARGB(255, 243, 243, 243);
const LIGHT_PRIMARY = Color.fromARGB(255, 101, 209, 219);
const PRIMARY = Color.fromARGB(255, 0, 160, 200);
const LIGHT_ACCENT = Color.fromARGB(255, 169, 242, 144);
const ACCENT = Color.fromARGB(255, 142, 226, 117);
const DARK_GREY = Color.fromARGB(255, 112, 112, 112);
const TEXT_COLOR = Color.fromARGB(255, 75, 75, 75);
const LIGHT_GREY = Color.fromARGB(255, 225, 225, 225);
const GREY = Color.fromARGB(255, 194, 194, 194);
const ERROR_COLOR = Color.fromARGB(255, 243, 67, 54);
const SHADOW_COLOR = Color.fromARGB(255, 200, 200, 200);
