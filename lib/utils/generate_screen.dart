import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:todo_quiz/screens/home/home.dart';
import 'package:todo_quiz/screens/login/login.dart';
import 'package:todo_quiz/screens/splash/splash.dart';

class GenerateScreen {
  static Route<dynamic> onGenerate(RouteSettings value) {
    switch (value.name) {
      case RouteNames.SPLASH:
        return MaterialPageRoute(builder: (context) => Splash());
      case RouteNames.LOGIN:
        return CupertinoPageRoute(builder: (ctx) => Login());
      case RouteNames.HOME:
        return CupertinoPageRoute(builder: (ctx) => Home());

      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: Text('Routing Error'),
          ),
          body: Center(
            child: Text('No route found!'),
          ),
        );
      },
    );
  }
}

class RouteNames {
  static const String SPLASH = "/";
  static const String HOME = "/home";
  static const String LOGIN = "/login";
}
