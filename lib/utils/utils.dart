import 'package:flutter/material.dart';
import 'package:todo_quiz/utils/colors.dart';

showSnackBarMessage(
    ScaffoldState _scaffoldState,
    String message, {
      Duration duration = const Duration(milliseconds: 2000),
    }) {
  _scaffoldState.showSnackBar(
    new SnackBar(
      duration: duration,
      content: Text(
        message,
        style: TextStyle(
          fontSize: 16,
          color: Colors.white,
          fontWeight: FontWeight.w600,
        ),
      ),
      backgroundColor: DARK_GREY,
    ),
  );
}

Future<void> showConfirmDialog(BuildContext context, String message, Function onConfirm) async {
  await showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        content: Text(
          message,
          style: TextStyle(
            color: DARK_GREY,
            fontSize: 16,
          ),
        ),
        actions: [
          FlatButton(
            child: Text(
              'Cancel',
              style: TextStyle(
                color: GREY,
                fontWeight: FontWeight.w600,
                fontSize: 14,
              ),
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          FlatButton(
            child: Text(
              'Ok',
              style: TextStyle(
                color: PRIMARY,
                fontWeight: FontWeight.w600,
                fontSize: 14,
              ),
            ),
            onPressed: () {
              Navigator.pop(context);
              onConfirm();
            },
          ),
        ],
      );
    },
  );
}