import 'package:flutter/material.dart';
import 'package:todo_quiz/injection_container.dart';
import 'package:todo_quiz/utils/colors.dart';
import 'package:todo_quiz/utils/generate_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: RouteNames.SPLASH,
      debugShowCheckedModeBanner: false,
      onGenerateRoute: GenerateScreen.onGenerate,
      theme: ThemeData(
        primaryColor: PRIMARY,
        textTheme: TextTheme(
          headline1: TextStyle(
            fontFamily: 'Poppins',
          ),
        ),
      ),
    );
  }
}
