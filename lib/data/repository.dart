import 'package:todo_quiz/data/db_helper.dart';
import 'package:todo_quiz/data/entities/todo_item.dart';
import 'package:todo_quiz/data/prefs_helper.dart';
import 'package:todo_quiz/utils/constants.dart';
import 'package:meta/meta.dart';

class Repository {
  final DatabaseHelper _dbHelper;
  final PrefsHelper _prefsHelper;

  Repository(this._dbHelper, this._prefsHelper);

  Future<bool> login(String username, String password) async {
    await Future.delayed(Duration(milliseconds: 1000));
    final loginResult =
        username == LOGIN_USERNAME && password == LOGIN_PASSWORD;
    if (loginResult) _prefsHelper.startSession(username);
    return loginResult;
  }

  Future<List<TodoTask>> getTodoList() async {
    final list = await _dbHelper.getTodoTasks();
    return list
        .map(
          (task) => TodoTask(
            id: task[DatabaseConstants.COL_ID],
            title: task[DatabaseConstants.COL_TITLE],
            description: task[DatabaseConstants.COL_DESCRIPTION],
            date: task[DatabaseConstants.COL_CREATION_DATE],
            hasDone: task[DatabaseConstants.COL_HAS_DONE] == 1,
          ),
        )
        .toList();
  }

  Future<TodoTask> addNewTask({
    @required String title,
    @required String description,
  }) async {
    final task = await _dbHelper.insertTodoTask(
      title: title,
      description: description,
    );

    if (task != null)
      return TodoTask(
        id: task[DatabaseConstants.COL_ID],
        title: task[DatabaseConstants.COL_TITLE],
        description: task[DatabaseConstants.COL_DESCRIPTION],
        date: task[DatabaseConstants.COL_CREATION_DATE],
        hasDone: task[DatabaseConstants.COL_HAS_DONE] == 1,
      );
    throw Exception('Failed to add new task!');
  }

  Future<TodoTask> updateTaskStatus({
    @required int taskId,
    @required bool hasDone,
  }) async {
    final task =
        await _dbHelper.updateTaskStatus(taskId: taskId, hasDone: hasDone);

    if (task != null)
      return TodoTask(
        id: task[DatabaseConstants.COL_ID],
        title: task[DatabaseConstants.COL_TITLE],
        description: task[DatabaseConstants.COL_DESCRIPTION],
        date: task[DatabaseConstants.COL_CREATION_DATE],
        hasDone: task[DatabaseConstants.COL_HAS_DONE] == 1,
      );
    throw Exception('Failed to update task status!');
  }

  Future<TodoTask> updateTaskDetails({
    @required int taskId,
    @required String title,
    @required String description,
  }) async {
    final task = await _dbHelper.updateTaskDetails(
      title: title,
      taskId: taskId,
      description: description,
    );

    if (task != null)
      return TodoTask(
        id: task[DatabaseConstants.COL_ID],
        title: task[DatabaseConstants.COL_TITLE],
        description: task[DatabaseConstants.COL_DESCRIPTION],
        date: task[DatabaseConstants.COL_CREATION_DATE],
        hasDone: task[DatabaseConstants.COL_HAS_DONE] == 1,
      );
    throw Exception('Failed to update task status!');
  }

  Future<bool> removeTask({@required int taskId}) async =>
      await _dbHelper.removeTodoTask(taskId: taskId);
}
