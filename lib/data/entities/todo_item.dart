import 'package:meta/meta.dart';

class TodoTask {
  final int id;
  final String title;
  final String description;
  final String date;
  bool hasDone;

  TodoTask({
    @required this.id,
    @required this.title,
    @required this.description,
    @required this.date,
    @required this.hasDone,
  });
}
