import 'package:sqflite/sqflite.dart';
import 'package:meta/meta.dart';
import 'package:todo_quiz/utils/constants.dart';

class DatabaseHelper {
  final Database database;

  DatabaseHelper(this.database);

  Future<List<Map<String, dynamic>>> getTodoTasks() async {
    return await database.transaction<List<Map<String, dynamic>>>(
      (txn) async {
        final query = '''SELECT * FROM ${DatabaseConstants.TABLE_TODO_ITEM}''';
        List<Map<String, dynamic>> list = await txn.rawQuery(query);
        return list;
      },
    );
  }

  Future<bool> removeTodoTask({
    @required int taskId,
  }) async {
    return await database.transaction<bool>(
      (txn) async {
        final query = '''DELETE FROM ${DatabaseConstants.TABLE_TODO_ITEM} 
            WHERE ${DatabaseConstants.COL_ID} = ?''';
        final rowDeleted = await txn.rawDelete(query, [taskId]);
        print('DatabaseHelper => removeTodoItem => rowDeleted = $rowDeleted');
        return rowDeleted > 0;
      },
    );
  }

  Future<Map<String, dynamic>> insertTodoTask({
    @required String title,
    @required String description,
  }) async {
    final taskId = await database.transaction<int>(
      (txn) async {
        final query = '''INSERT INTO ${DatabaseConstants.TABLE_TODO_ITEM}(
          ${DatabaseConstants.COL_TITLE}, 
          ${DatabaseConstants.COL_DESCRIPTION}, 
          ${DatabaseConstants.COL_CREATION_DATE},
          ${DatabaseConstants.COL_HAS_DONE}
          ) VALUES(?, ?, ?, ?)''';
        return await txn.rawInsert(
            query, [title, description, DateTime.now().toString(), 0]);
      },
    );
    return await getTaskById(taskId: taskId);
  }

  Future<Map<String, dynamic>> updateTaskStatus({
    @required int taskId,
    @required bool hasDone,
  }) async {
     await database.transaction(
      (txn) async {
        final query = '''UPDATE ${DatabaseConstants.TABLE_TODO_ITEM} 
          SET ${DatabaseConstants.COL_HAS_DONE} = ? 
          WHERE ${DatabaseConstants.COL_ID} = ?''';
        await txn.rawUpdate(query, [hasDone ? 1 : 0, taskId]);
      },
    );
    return await getTaskById(taskId: taskId);
  }

  Future<Map<String, dynamic>> updateTaskDetails({
    @required int taskId,
    @required String title,
    @required String description,
  }) async {
     await database.transaction(
      (txn) async {
        final query = '''UPDATE ${DatabaseConstants.TABLE_TODO_ITEM} 
          SET ${DatabaseConstants.COL_TITLE} = ?,
          ${DatabaseConstants.COL_DESCRIPTION} = ?
          WHERE ${DatabaseConstants.COL_ID} = ?''';
        await txn.rawUpdate(query, [title, description, taskId]);
      },
    );
    return await getTaskById(taskId: taskId);
  }

  Future<Map<String, dynamic>> getTaskById({
    @required int taskId,
  }) async {
    return await database.transaction<Map<String, dynamic>>(
      (txn) async {
        final query = '''SELECT * FROM ${DatabaseConstants.TABLE_TODO_ITEM}
          WHERE ${DatabaseConstants.COL_ID} = ?''';
        final result = await txn.rawQuery(query, [taskId]);
        print('DatabaseHelper => getTaskById => result count = ${result.length}');
        return result.isNotEmpty ? result.first : null;
      },
    );
  }
}

Future<Database> initDatabase() async {
  return await openDatabase(
    'todo_test_db.db',
    version: 1,
    onCreate: (Database db, int version) async {
      String createItemTableQuery =
          '''CREATE TABLE ${DatabaseConstants.TABLE_TODO_ITEM}(
            ${DatabaseConstants.COL_ID} INTEGER PRIMARY KEY,
            ${DatabaseConstants.COL_TITLE} TEXT, 
            ${DatabaseConstants.COL_DESCRIPTION} TEXT, 
            ${DatabaseConstants.COL_CREATION_DATE} TEXT, 
            ${DatabaseConstants.COL_HAS_DONE} INTEGER)''';
      await db.execute(createItemTableQuery);
    },
  );
}
