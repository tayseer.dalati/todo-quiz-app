import 'package:shared_preferences/shared_preferences.dart';
import 'package:todo_quiz/utils/constants.dart';

class PrefsHelper {
  final SharedPreferences _prefs;

  PrefsHelper(this._prefs);

  Future<void> startSession(String username) async {
    await _prefs.setString(PreferencesKeys.USERNAME, username);
    await _prefs.setBool(PreferencesKeys.IS_LOGGED_IN, true);
  }
}