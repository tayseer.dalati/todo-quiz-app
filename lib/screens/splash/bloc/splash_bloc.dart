import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:todo_quiz/data/repository.dart';

part 'splash_event.dart';

part 'splash_state.dart';

class SplashBloc extends Bloc<SplashEvent, SplashState> {
  final Repository _repository;

  SplashBloc(this._repository);

  @override
  SplashState get initialState => SplashInitial();

  @override
  Stream<SplashState> mapEventToState(SplashEvent event) async* {
    if (event is ScreenAppeared) {
      await Future.delayed(Duration(seconds: 2));
      yield SkipScreen();
    }
  }
}
