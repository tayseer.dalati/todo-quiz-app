import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_quiz/injection_container.dart';
import 'package:todo_quiz/utils/generate_screen.dart';

import 'bloc/splash_bloc.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  final _bloc = sl<SplashBloc>();

  @override
  void initState() {
    super.initState();
    _bloc.add(ScreenAppeared());
  }

  @override
  void dispose() {
    _bloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: _bloc,
      listener: (_, state) {
        if (state is SkipScreen)
          Navigator.pushReplacementNamed(context, RouteNames.LOGIN);
      },
      child: Container(
        color: Colors.white,
        child: Center(
          child: Image.asset('assets/images/ic_todo.jpg'),
        ),
      ),
    );
  }
}
