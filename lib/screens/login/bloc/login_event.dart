part of 'login_bloc.dart';

@immutable
abstract class LoginEvent {}

class SubmitLogin extends LoginEvent {
  final String username;
  final String password;

  SubmitLogin({
    @required this.username,
    @required this.password,
  });
}
