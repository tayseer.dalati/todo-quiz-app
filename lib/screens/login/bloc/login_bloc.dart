import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:todo_quiz/data/repository.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final Repository _repository;

  LoginBloc(this._repository);

  @override
  LoginState get initialState => LoginInitial();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is SubmitLogin) yield* mapSubmitLogin(event);
  }

  Stream<LoginState> mapSubmitLogin(SubmitLogin event) async* {
    if (event.username.isEmpty || event.password.isEmpty) return;
    yield LoggingIn();
    final result = await _repository.login(event.username, event.password);
    yield result ? LoginSucceed() : LoginError();
  }
}
