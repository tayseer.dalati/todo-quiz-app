import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_quiz/injection_container.dart';
import 'package:todo_quiz/screens/login/bloc/login_bloc.dart';
import 'package:todo_quiz/screens/login/login_input_field.dart';
import 'package:todo_quiz/utils/colors.dart';
import 'package:todo_quiz/utils/generate_screen.dart';
import 'package:todo_quiz/utils/utils.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _bloc = sl<LoginBloc>();
  TextEditingController _usernameController;
  TextEditingController _passwordController;
  FocusNode _usernameFocusNode;
  FocusNode _passwordFocusNode;
  Size screenSize;

  @override
  void initState() {
    _usernameController = TextEditingController();
    _passwordController = TextEditingController();
    _usernameFocusNode = FocusNode();
    _passwordFocusNode = FocusNode();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    screenSize = MediaQuery.of(context).size;
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _bloc.close();
    _usernameController.dispose();
    _passwordController.dispose();
    _usernameFocusNode.dispose();
    _passwordFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: WHITE,
      body: BlocListener(
        bloc: _bloc,
        listener: (_, state) {
          if (state is LoginSucceed)
            Navigator.pushReplacementNamed(context, RouteNames.HOME);
          else if (state is LoginError)
            showSnackBarMessage(
              _scaffoldKey.currentState,
              'Invalid username or password!',
            );
        },
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 8.0),
              Expanded(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 24.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        _headerView,
                        Column(
                          children: <Widget>[
                            LoginInputField(
                              title: 'Username',
                              controller: _usernameController,
                              focusNode: _usernameFocusNode,
                              nextFocusNode: _passwordFocusNode,
                            ),
                            SizedBox(height: 24.0),
                            LoginInputField(
                              title: 'Password',
                              obscureText: true,
                              onSubmit: _onSubmit,
                              controller: _passwordController,
                              focusNode: _passwordFocusNode,
                            ),
                            SizedBox(height: 32.0),
                            submitButton,
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget get submitButton => BlocBuilder(
    bloc: _bloc,
    builder: (_, state) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          FlatButton(
            onPressed: _onSubmit,
            child: Text(
              'Login',
              style: TextStyle(
                fontSize: 21,
                color: PRIMARY,
                fontWeight: FontWeight.bold,
                fontFamily: Theme.of(context).textTheme.headline1.fontFamily,
              ),
            ),
          ),
          Visibility(
            visible: state is LoggingIn,
            child: SizedBox(
              width: 14.0,
              height: 14.0,
              child: CircularProgressIndicator(
                strokeWidth: 1.5,
                valueColor: AlwaysStoppedAnimation(PRIMARY),
              ),
            ),
          ),
        ],
      );
    },
  );

  Widget get _headerView => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 32.0),
          Image.asset(
            'assets/images/ic_login.jpg',
            matchTextDirection: false,
            height: screenSize.width * 0.3,
            fit: BoxFit.contain,
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 8.0, top: 24.0),
            child: Text(
              'Process with your',
              style: TextStyle(
                fontSize: 24,
                color: DARK_GREY,
                fontWeight: FontWeight.w300,
                fontFamily: Theme.of(context).textTheme.headline1.fontFamily,
              ),
            ),
          ),
          Text(
            'Login',
            style: TextStyle(
              fontSize: 42,
              color: DARK_GREY,
              fontWeight: FontWeight.bold,
              fontFamily: Theme.of(context).textTheme.headline1.fontFamily,
            ),
          ),
          SizedBox(height: 48.0),
        ],
      );

  void _onSubmit() {
    FocusScope.of(context).unfocus();
    _bloc.add(
      SubmitLogin(
        username: _usernameController.text.trim(),
        password: _passwordController.text.trim(),
      ),
    );
  }
}
