import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_quiz/data/entities/todo_item.dart';
import 'package:todo_quiz/screens/home/bloc/home_bloc.dart';
import 'package:todo_quiz/utils/colors.dart';
import 'package:todo_quiz/utils/expand_section.dart';
import 'package:todo_quiz/utils/utils.dart';

class TaskTile extends StatefulWidget {
  final TodoTask task;
  final VoidCallback onRemove;
  final VoidCallback onEdit;

  TaskTile({this.task, this.onRemove, this.onEdit});

  @override
  _TaskTileState createState() => _TaskTileState();
}

class _TaskTileState extends State<TaskTile> {
  bool expanded = false;

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      key: Key(widget.task.id.toString()),
      direction: DismissDirection.horizontal,
      background: Container(color: Colors.black12),
      confirmDismiss: (_) async {
        var confirmed = false;
        await showConfirmDialog(
          context,
          'Please confirm delete this task?',
          () => confirmed = true,
        );
        return confirmed;
      },
      onDismissed: (direction) => widget.onRemove(),
      child: InkWell(
        onTap: () {
          setState(() {
            expanded = !expanded;
          });
        },
        child: Card(
          color: WHITE,
          elevation: 4.0,
          margin: EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(8.0))),
          child: Row(
            children: [
              Checkbox(
                  value: widget.task.hasDone,
                  onChanged: (status) => BlocProvider.of<HomeBloc>(context)
                      .add(ChangeTaskStatus(widget.task.id, status))),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.task.title,
                        style: TextStyle(
                          color: DARK_GREY,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          fontFamily:
                              Theme.of(context).textTheme.headline1.fontFamily,
                        ),
                      ),
                      ExpandedSection(
                        expand: expanded,
                        child: Text(
                          widget.task.description,
                          style: textStyle,
                        ),
                      ),
                      SizedBox(height: 8.0),
                      Text(
                        widget.task.date,
                        style: textStyle.copyWith(fontSize: 12, color: GREY),
                      ),
                      ExpandedSection(
                        expand: expanded,
                        child: Align(
                          alignment: Alignment.bottomRight,
                          child: FlatButton(
                            onPressed: widget.onEdit,
                            child: Text(
                              'Edit',
                              style: textStyle.copyWith(
                                fontSize: 16,
                                color: Colors.orange,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  TextStyle get textStyle => TextStyle(
        color: DARK_GREY,
        fontSize: 14,
        fontFamily: Theme.of(context).textTheme.headline1.fontFamily,
      );
}
