part of 'home_bloc.dart';

@immutable
abstract class HomeState {}

class HomeInitial extends HomeState {}

class UpdateList extends HomeState {}

class InsertNewItem extends HomeState {}

class InsertAllItems extends HomeState {}

class EmptyList extends HomeState {}

class RemoveListItem extends HomeState {
  final TodoTask task;
  final int index;

  RemoveListItem(this.task, this.index);
}

class ShowError extends HomeState {
  final String errorMessage;

  ShowError(this.errorMessage);
}
