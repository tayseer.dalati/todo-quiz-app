import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:todo_quiz/data/entities/todo_item.dart';
import 'package:todo_quiz/data/repository.dart';

part 'home_event.dart';

part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final Repository _repository;

  HomeBloc(this._repository);

  List<TodoTask> _list = [];

  List<TodoTask> get todoList => _list;

  @override
  HomeState get initialState => HomeInitial();

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    if (event is GetTodoList) {
      _list.clear();
      _list.addAll((await _repository.getTodoList()).reversed);
      yield _list.isNotEmpty ? InsertAllItems() : EmptyList();
    } else if (event is AddNewTask)
      yield* mapAddNewTask(event);
    else if (event is ChangeTaskStatus)
      yield* mapChangeTaskStatus(event);
    else if (event is EditTaskDetails)
      yield* mapEditTaskDetails(event);
    else if (event is RemoveTask) yield* mapRemoveTask(event);
  }

  Stream<HomeState> mapRemoveTask(RemoveTask event) async* {
    final result = await _repository.removeTask(taskId: event.taskId);
    final index = _list.indexWhere((element) => element.id == event.taskId);
    if (result) {
      yield RemoveListItem(_list[index], index);
      _list.removeAt(index);
    }
  }

  Stream<HomeState> mapEditTaskDetails(EditTaskDetails event) async* {
    try {
      final task = await _repository.updateTaskDetails(
        taskId: event.taskId,
        title: event.title,
        description: event.description,
      );
      final index = _list.indexWhere((element) => element.id == event.taskId);
      _list[index] = task;
      yield UpdateList();
    } catch (e) {
      yield ShowError(e.toString());
    }
  }

  Stream<HomeState> mapChangeTaskStatus(ChangeTaskStatus event) async* {
    try {
      final task = await _repository.updateTaskStatus(
          taskId: event.taskId, hasDone: event.status);
      final index = _list.indexWhere((element) => element.id == event.taskId);
      _list[index] = task;
      yield UpdateList();
    } catch (e) {
      yield ShowError(e.toString());
    }
  }

  Stream<HomeState> mapAddNewTask(AddNewTask event) async* {
    try {
      final task = await _repository.addNewTask(
        title: event.title,
        description: event.description,
      );
      _list.insert(0, task);
      yield InsertNewItem();
    } catch (e) {
      yield ShowError(e.toString());
    }
  }
}
