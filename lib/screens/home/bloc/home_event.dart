part of 'home_bloc.dart';

@immutable
abstract class HomeEvent {}

class GetTodoList extends HomeEvent {}

class RemoveTask extends HomeEvent {
  final int taskId;

  RemoveTask(this.taskId);
}

class ChangeTaskStatus extends HomeEvent {
  final int taskId;
  final bool status;

  ChangeTaskStatus(this.taskId, this.status);
}

class EditTaskDetails extends HomeEvent {
  final int taskId;
  final String title;
  final String description;

  EditTaskDetails(this.taskId, this.title, this.description);
}

class AddNewTask extends HomeEvent {
  final String title;
  final String description;

  AddNewTask(this.title, this.description);
}