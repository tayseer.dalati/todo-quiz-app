import 'package:flutter/material.dart';
import 'package:todo_quiz/data/entities/todo_item.dart';
import 'package:todo_quiz/utils/colors.dart';

class TaskEditorView extends StatefulWidget {
  final Function(String, String) onSubmit;
  final TodoTask task;
  final bool isNew;

  TaskEditorView({
    @required this.onSubmit,
    this.isNew = false,
    this.task,
  });

  @override
  _TaskEditorViewState createState() => _TaskEditorViewState();
}

class _TaskEditorViewState extends State<TaskEditorView> {
  TextEditingController _titleController;
  TextEditingController _descriptionController;

  @override
  void initState() {
    _titleController = TextEditingController(
        text: widget.task != null ? widget.task.title : '');
    _descriptionController = TextEditingController(
        text: widget.task != null ? widget.task.description : '');
    super.initState();
  }

  @override
  void dispose() {
    _titleController.dispose();
    _descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: MediaQuery.of(context).viewInsets.copyWith(
            left: 16.0,
            right: 16.0,
          ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 12.0),
          taskInputField(
            labelText: 'Title',
            controller: _titleController,
          ),
          Divider(),
          taskInputField(
            labelText: 'Description',
            controller: _descriptionController,
            maxLines: 4,
          ),
          Row(
            children: [
              Expanded(
                child: FlatButton(
                  onPressed: () => Navigator.pop(context),
                  child: Text(
                    'Cancel',
                    style: TextStyle(
                      color: DARK_GREY,
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: FlatButton(
                  onPressed: () {
                    widget.onSubmit(
                        _titleController.text, _descriptionController.text);
                    Navigator.pop(context);
                  },
                  child: Text(
                    'Submit',
                    style: TextStyle(
                      color: PRIMARY,
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget taskInputField({
    String labelText,
    int maxLines = 1,
    TextEditingController controller,
  }) {
    return TextField(
      maxLines: maxLines,
      controller: controller,
      decoration: InputDecoration(
        border: InputBorder.none,
        labelText: labelText,
        labelStyle: TextStyle(
          fontSize: 15,
          color: GREY,
          fontWeight: FontWeight.w600,
          fontFamily: Theme.of(context).textTheme.headline1.fontFamily,
        ),
      ),
    );
  }
}
